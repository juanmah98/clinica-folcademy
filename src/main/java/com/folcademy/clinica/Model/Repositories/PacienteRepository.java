package com.folcademy.clinica.Model.Repositories;



import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

//@Repository
//public interface PacienteRepository extends PagingAndSortingRepository<Paciente, Integer> {
//
//    Page<Paciente> findAllByApellido(String apellido, Pageable pageable);
//    // SELECT * FROM Paciente where Apellido = @apellido
//
//}

@Repository
public interface PacienteRepository  extends JpaRepository<Paciente, Integer> {
}
