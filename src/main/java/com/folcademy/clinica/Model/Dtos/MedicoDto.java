package com.folcademy.clinica.Model.Dtos;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicoDto {
    Integer id;
    //String nombre; No hace falta asignar el nombre, al asignar "" en el Medico.java (constructor)
    //String apellido;
    @NotNull //Esto sirve para sabe que no son nullos los datos
    String profession;
    int consulta;
}
