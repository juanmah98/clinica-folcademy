package com.folcademy.clinica.Services;


import com.folcademy.clinica.Controllers.MedicoController;
import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;

import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRespoistory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service("medicoService")
public class MedicoService {
    private  final MedicoRespoistory medicoRepository;
    private final MedicoMapper medicoMapper; //Conectamos el Mapper

    public MedicoService(MedicoRespoistory medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    public List<MedicoDto> listarTodos(){ //Tenemos las funciones conectadas al mapper ahora a traves del Dto
       return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }

    public MedicoDto listarUno(Integer id){
        if(!medicoRepository.existsById(id))
            throw new RuntimeException("No existe el medico");
        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);
    }

    public MedicoEnteroDto agregar(MedicoEnteroDto entity){
       entity.setId(null);
       if (entity.getConsulta()<0)
           throw new BadRequestException("La consulta no peude ser menor a 0");
       if (entity.getProfesion()=="")
           throw new BadRequestException("La profecion no puede estar vacia");

        return medicoMapper.entityToEnteroDto(medicoRepository.save(medicoMapper.enteroDtoToEntity(entity)));
    }


    public MedicoEnteroDto editar(Integer idMedico, MedicoEnteroDto dto){
        if(!medicoRepository.existsById(idMedico))
            throw  new RuntimeException("No existe el medico");
        if (dto.getConsulta()<0)
            throw new BadRequestException("La consulta no peude ser menor a 0");
        if (dto.getProfesion()=="")
            throw new BadRequestException("La profecion no puede estar vacia");
        dto.setId(idMedico);
//        dto->entidad;
//        guardar;
//        entidad->dto;
        return
                medicoMapper.entityToEnteroDto(
                        medicoRepository.save(
                                medicoMapper.enteroDtoToEntity(
                                        dto
                                )
                        )
                );

    }

    public boolean eliminar(Integer id){
        if (!medicoRepository.existsById(id))
             throw new RuntimeException("No existe el medico");
        medicoRepository.deleteById(id);
        return true;
    }


}
