package com.folcademy.clinica.Services;


import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;


import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;

import com.folcademy.clinica.Model.Repositories.PacienteRepository;

import org.springframework.beans.factory.config.TypedStringValue;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;
//@Service
//public class PacienteService implements IPacienteService {
//
//    private final PacienteRepository pacienteRepository;
//
//    public PacienteService(PacienteRepository pacienteRepository) {
//        this.pacienteRepository = pacienteRepository;
//    }
//
//    @Override
//    public List<Paciente> findAllPacientes() {
//
//       return (List<Paciente>) pacienteRepository.findAll();
//
//    }
//
//    @Override
//    public List<Paciente> findPacienteById(Integer id) {
//        return null;
//    }
//
//
//
//
//}

@Service("PacienteSerive")
public class PacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;


    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper)
    {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }

    public List<PacienteDto> listarTodos(){
        return pacienteRepository.findAll().stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
    }

    public PacienteDto listarUno(Integer id){
        if (!pacienteRepository.existsById(id))
            throw new RuntimeException("No existe el paciente");

        return pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null);
    }

    public PacienteEnteroDto agregar(PacienteEnteroDto entity){
        entity.setId(null);
        if (entity.getDni()=="")
            throw new NotFoundException("Error Datos vacios");
        if (entity.getTelefono()=="")
            throw new BadRequestException("El Telefono no puede estar vacio");

        return pacienteMapper.entityToEnteroDto(pacienteRepository.save(pacienteMapper.enteroDtoToEntity(entity)));
    }


    public PacienteEnteroDto editar(Integer idPaciente, PacienteEnteroDto dto){
        if(!pacienteRepository.existsById(idPaciente))
            throw new RuntimeException("No existe el paciente");
        if (dto.getDni()=="")
            throw new NotFoundException("Error Datos vacios");
        if (dto.getTelefono()=="")
            throw new BadRequestException("El Telefono no puede estar vacio");
        dto.setId(idPaciente);
//        dto->entidad;
//        guardar;
//        entidad->dto;
        return
                pacienteMapper.entityToEnteroDto(
                        pacienteRepository.save(
                                pacienteMapper.enteroDtoToEntity(
                                 dto
                                )
                        )
                );

    }

    public boolean eliminar(Integer id){
        if (!pacienteRepository.existsById(id))
            throw new RuntimeException("No existe el paciente");
        pacienteRepository.deleteById(id);
        return true;
    }

}
