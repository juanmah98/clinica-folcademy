package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;

import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/medicos")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    @PreAuthorize("hasAnyAuthority('get_medico','get_medicos')")
    @GetMapping("") //Nos devuelve toda la lista de medicos, pero al ser Dto, solo lo que a el Dto se le asigne
    public ResponseEntity<List<MedicoDto>>listarTodo(){
        return ResponseEntity.ok(medicoService.listarTodos());
    }

    @PreAuthorize("hasAnyAuthority('get_medico','get_medicos')")
    @GetMapping("/{idMedico}") //Nos devuelve solo 1 medico
    public  ResponseEntity<MedicoDto> listarUno(@PathVariable(name = "idMedico") int id){
        return ResponseEntity.ok(medicoService.listarUno(id));
    }

    @PreAuthorize("hasAnyAuthority('post','post_medicos')")
    @PostMapping("")//Podemos Agregar un medico, la sigla "" se distingue si es get o post en el servicio.
    public ResponseEntity<MedicoEnteroDto> agregar(@RequestBody @Validated MedicoEnteroDto entity){ //Utilizar si o si @Validated
        return ResponseEntity.ok(medicoService.agregar(entity));               //Nos va a servir para futuros errores
    }                                                               //Valdia por ej, el int de id

    @PreAuthorize("hasAuthority('put_medicos')")
    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoEnteroDto> editar(@PathVariable(name = "idMedico")int id,
                                                    @RequestBody  MedicoEnteroDto dto){
        return ResponseEntity.ok(medicoService.editar(id,dto));
    }

    @PreAuthorize("hasAuthority('del_medicos')")
    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idMedico")int id)
                                                                {
        return ResponseEntity.ok(medicoService.eliminar(id));
    }


}
