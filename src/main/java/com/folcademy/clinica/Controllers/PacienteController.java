package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;

import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping ( "/pacientes")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    @PreAuthorize("hasAnyAuthority('get_pacientes','get_paciente')")
    @GetMapping("")
    public ResponseEntity<List<PacienteDto>>listarTodo(){
        return ResponseEntity.ok(pacienteService.listarTodos());
    }

    @PreAuthorize("hasAnyAuthority('get_pacientes','get_paciente')")
    @GetMapping("/{idPaciente}") //Nos devuelve solo 1 medico
    public  ResponseEntity<PacienteDto> listarUno(@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity.ok(pacienteService.listarUno(id));
    }

    @PreAuthorize("hasAuthority('post_pacientes')")
    @PostMapping("")
    public ResponseEntity<PacienteEnteroDto> agregar(@RequestBody  @Validated PacienteEnteroDto entity){
        return ResponseEntity.ok(pacienteService.agregar(entity));
    }


    @PreAuthorize("hasAuthority('put_pacientes')")
    @PutMapping("/{idPaciente}")
    public ResponseEntity<PacienteEnteroDto> editar(@PathVariable(name = "idPaciente")int id,
                                                    @RequestBody PacienteEnteroDto dto){
        return ResponseEntity.ok(pacienteService.editar(id,dto));
    }

    @PreAuthorize("hasAuthority('del_pacientes')")
    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idPaciente")int id)
    {
        return ResponseEntity.ok(pacienteService.eliminar(id));
    }

}
