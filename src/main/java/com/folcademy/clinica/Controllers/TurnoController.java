
package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping ("/turnos")
public class TurnoController {

    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }


    @PreAuthorize("hasAnyAuthority('get_turnos','get_turno')" )
    @GetMapping(value = "")
    public ResponseEntity<List<Turno>>findAll() {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findAllTurnos()
                );
    }

    @PreAuthorize("hasAnyAuthority('get_turnos','get_turno')")
    @GetMapping("/{idTurno}")
    public  ResponseEntity<TurnoEnteroDto> listarUno(@PathVariable(name = "idTurno") int id){
        return ResponseEntity.ok(turnoService.listarUno(id));
    }

    @PreAuthorize("hasAuthority('post_turnos')")
    @PostMapping("")
    public ResponseEntity<TurnoEnteroDto> agregar(@RequestBody @Validated TurnoEnteroDto entity){
        return ResponseEntity.ok(turnoService.agregar(entity));
    }

    @PreAuthorize("hasAuthority('put_turnos')")
    @PutMapping("/{idTurno}")
    public ResponseEntity<TurnoEnteroDto> editar(@PathVariable(name = "idTurno")int id,
                                                  @RequestBody  TurnoEnteroDto dto){
        return ResponseEntity.ok(turnoService.editar(id,dto));
    }

    @PreAuthorize("hasAuthority('del_turnos')")
    @DeleteMapping("/{idTurno}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idTurno")int id)
    {
        return ResponseEntity.ok(turnoService.eliminar(id));
    }



}
